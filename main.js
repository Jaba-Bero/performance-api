/* eslint-disable no-restricted-syntax */
/* eslint-disable linebreak-style */
/* eslint-disable no-unused-vars */
/* eslint-disable import/extensions */
/* eslint-disable no-mixed-operators */
/* eslint-disable no-plusplus */

import './styles/loadedpage.css';
import './styles/style.css';
import { community } from './community.js';

window.addEventListener('load', community());

const template = document.createElement('template');
template.innerHTML = `
  <style>
    .invisible { display: none }
    button {
      padding: 0px 0px;
      border: 0px;
      margin 0px 0px;
      background: none;
    }
    .app-section
  </style>
  <div> <slot name="div" /> </div>
  <h1> <slot name="title" /> </h1>
  <h2> <slot name="subtitle" /> </h2>
  <h2> <slot name="h3" /> </h2>
  <article> <slot name="article" /> </article>
  <button> <slot name="btn" /> </button>
  <form> <slot name="form" /> </form>
`;
class WebsiteSection extends HTMLElement {
  constructor() {
    super();
    this.attachShadow({ mode: 'open' });
    this.shadowRoot.appendChild(template.content.cloneNode(true));
  }
}

window.customElements.define('website-section', WebsiteSection);

// Web-worker task
function sendRequest(info) {
  const xhr = new XMLHttpRequest();
  xhr.open('post', 'api/analytics/user');
  xhr.responseType = 'json';
  xhr.setRequestHeader('Content-Type', 'Application/json');
  xhr.onload = () => {
    const data = xhr.response;
    if (xhr.status == 422) {
      window.alert(JSON.stringify(data));
    } else {
      console.log(data);
    }
  };
  xhr.onerror = (error) => {
    console.log(error);
  };
  xhr.send(JSON.stringify({ info }));
  console.log(`${info} is sent`);
}

function sendPerformanceRequest() {
  const xhr = new XMLHttpRequest();
  xhr.open('post', 'api/analytics/performance');
  xhr.responseType = 'json';
  xhr.setRequestHeader('Content-Type', 'Application/json');
  xhr.send();
}

sendPerformanceRequest();

if (window.Worker) {
  const btns = document.getElementsByClassName('app-section__button');
  const input = document.getElementById('email-input');
  const url = new URL('./worker.js', import.meta.url);
  const worker = new Worker(url);

  for (let i = 0; i < btns.length; i++) {
    // eslint-disable-next-line no-loop-func
    btns[i].addEventListener('click', function () {
      worker.postMessage(this.textContent);
      console.log(this.textContent);
      worker.onmessage = (e) => {
        sendRequest(e.data);
      };
    });
  }
  input.addEventListener('click', function () {
    worker.postMessage(this.textContent);
    console.log(this.textContent);
    worker.onmessage = (e) => {
      sendRequest(e.data);
    };
  });
}

function perf(type, name, data) {
  console.log(`${type}: ${name} | ${data || ''}`);
}

window.addEventListener('load', () => {
  // performacne of server
  const entries = performance.getEntriesByType('measure');
  entries.forEach((entry) => {
    perf('mark', entry.name, entry.duration); // community performance
  });

  const navEntries = performance.getEntriesByType('navigation');
  navEntries.forEach((entry) => {
    const ttfb = entry.responseStart - entry.fetchStart;
    perf('navigation', 'load-page', entry.responseStart - entry.fetchStart);
    perf('memory', 'memory-usage/bytes', performance.memory.usedJSHeapSize);
  });

  for (const entryType of ['navigation', 'resource']) {
    for (const { name: url, serverTiming } of performance.getEntriesByType(
      entryType
    )) {
      if (
        serverTiming.length &&
        url === 'http://localhost:8080/api/analytics/performance'
      ) {
        console.log(serverTiming);
      }
    }
  }
});
